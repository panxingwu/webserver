package netsunion.com.webserver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

/** 
* @ClassName  : Process 
* @author     : panxingwu 
* @date       : 2017-1-17 下午4:20:14
* @Description: 请求处理类
*/
public class Process extends Thread {
	private Socket socket;
	private final String ROOT_PATH = "F:\\htdocs\\";
	private PrintStream out;

	public Process(Socket socket) {
		try {
			this.socket = socket;
			out = new PrintStream(socket.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		System.out.println("执行线程:"+Thread.currentThread().getName());
		try {
			InputStream is = socket.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String[] content = br.readLine().split(" ");
			if(content.length != 3){
				System.out.println("请求协议错误!");
				sendErrorMessage("500", "非法请求协议！");
				return;
			}
			System.out.println("Method:"+content[0]+",FileName:"+content[1]+",Version:"+content[2]);
			String fileName = content[1];
			sendFile(fileName);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/** 
	* @methodName  : sendErrorMessage 
	* @param       : @param errorCode
	* @param       : @param errorMessage 
	* @return      : void
	* @Description : 输出错误页面 
	*/
	public void sendErrorMessage(String errorCode,String errorMessage){
		//HTTP响应报头必须有固定格式
		out.println("HTTP/1.0 "+errorCode+" error");
		out.println("content-type:text/html");//必须有此属性，否则浏览器无法解析
		out.println();
		//HTTP响应报头必须有固定格式
		
		out.println("<html>");
		out.println(errorCode+" "+errorMessage);
		out.println("</html>");
		out.flush();
		out.close();
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/** 
	* @methodName  : sendFile 
	* @param       : @param fileName 
	* @return      : void
	* @Description : 输出请求的资源文件 
	*/
	public void sendFile(String fileName){
		File file = new File(ROOT_PATH+fileName);
		if(!file.exists()){
			System.out.println("文件未找到！");
			sendErrorMessage("404","文件未找到！");
			return;
		}else{
			try {
				FileInputStream in = new FileInputStream(file);
				byte[] content = new byte[(int)file.length()];
				in.read(content);//将文件流放入content数组
				//HTTP响应报头必须有固定格式
				out.println("HTTP/1.0 200 getFile");
				out.println("content-length:"+content.length);
				out.println();
				//HTTP响应报头必须有固定格式
				
				out.write(content);
				out.flush();
				out.close();
				in.close();
				socket.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
