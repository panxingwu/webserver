package netsunion.com.webserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/** 
* @ClassName  : Server 
* @author     : panxingwu 
* @date       : 2017-1-17 下午4:24:56
* @Description: 服务启动类
*/
public class Server {
	
	/** 
	* @methodName  : startServer 
	* @param       : @param port 
	* @return      : void
	* @Description : 启动服务 
	*/
	public void startServer(int port){
		try {
			ServerSocket serverSocket = new ServerSocket(port);
			while(true){
				Socket socket = serverSocket.accept();
				new Process(socket).start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		System.out.println("启动服务...");
		new Server().startServer(80);
	}
}
